entries = []
i = 0
until i == 5
  puts "Entrez un chiffre"
  entries[i] = gets.chomp!.to_i
  i += 1
end

max = entries.sort.reverse.first
somme = entries.reduce(:+) / entries.length

puts "Max : #{max}"
puts "Moyenne #{somme}"
